import enum
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

from .categories import CategoryOut

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


# class CategoryIn(BaseModel):
#     title: str


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class Clues(BaseModel):
    clues: list[ClueOut]


@router.get("/api/clues/{clue_id}", response_model=ClueOut)
def clues_list(clue_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT
                    cl.id,
                    cl.answer,
                    cl.question,
                    cl.value,
                    cl.invalid_count,
                    cl.canon,
                    ca.id AS cat_id,
                    ca.title,
                    ca.canon AS cat_canon
                FROM
                    clues cl
                    JOIN categories ca ON cl.category_id = ca.id
                WHERE
                    cl.id = %s;
                """,
                [clue_id]
            )
            row = cur.fetchone()
            category_fields = {
                "cat_id",
                "title",
                "cat_canon"
            }
            category = {}

            for i, column in enumerate(cur.description):
                if column.name in category_fields:
                    category[column.name] = row[i]
            category["id"] = category["cat_id"]
            category["canon"] = category["cat_canon"]

            record = {}
            for i, column in enumerate(cur.description):
                if column.name not in category_fields:
                    record[column.name] = row[i]
            record["category"] = category
            return record

